Welcome to my GitLab page. These are some of my projects:

* [Discord bot](https://gitlab.com/llsant/discord_bot): A Discord bot in Python that I develop and host for entertainment purposes.

* [Einstein's riddle in Z3](https://gitlab.com/llsant/einsteins-riddle): A solution for Einstein's riddle using the Z3 logic solver. 

* [UEFI Flappy Bird Mirror](https://gitlab.com/llsant/flappy-bird-uefi-backup): A mirror of a bare-metal UEFI game that
works like Flappy Bird, along with some convenience virtual machine files.

* [MS-DOS viruses](https://gitlab.com/llsant/ms-dos-viruses): A pack of traditional MS-DOS viruses I found somewhere, for entertainment purposes on a virtual machine.